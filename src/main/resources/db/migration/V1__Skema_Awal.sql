create table payment_notification(
  id varchar(36), 
  transaction_time timestamp not null,
  payment_reference varchar(100) not null,
  amount decimal(19,2) not null,
  primary key (id),
  unique (payment_reference)
);
